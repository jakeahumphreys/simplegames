﻿using System;

namespace SimpleGames.GuessTheNumber
{
    public static class GuessTheNumber
    {
        public static void Start()
        {
            var numberGuessed = false;
            var totalGuesses = 0;

            var numberToGuess = GenerateNumber();
            
            Console.WriteLine("Guess what number I'm thinking of? (Max 100)");

            while (numberGuessed == false)
            {
                var input = Console.ReadLine();

                if (input == "Exit")
                {
                    break;
                }

                var guess = int.TryParse(input, out var parsedGuess);
                
                if(guess == false)
                    Console.WriteLine("Sorry, I'm only thinking of a number!");

                if (parsedGuess > numberToGuess)
                {
                    totalGuesses++;
                    Console.WriteLine("Too High!");
                }

                if (parsedGuess < numberToGuess)
                {
                    totalGuesses++;
                    Console.WriteLine("Too Low!");
                }

                if (parsedGuess == numberToGuess)
                {
                    totalGuesses++;
                    Console.WriteLine("That's right!");
                    Console.WriteLine($"Guesses: {totalGuesses} \n");
                    numberGuessed = true;
                }
            }
        }

        private static int GenerateNumber()
        {
            var rnd = new Random();
            return rnd.Next(1, 100);
        }
    }
}