﻿using System;
using System.Collections.Generic;

namespace SimpleGames
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var shouldKeepRunning = true;

            while (shouldKeepRunning)
            {
                PrintMenu();
                
                var selection = Console.ReadLine();

                switch (selection)
                {
                    case "0":
                        shouldKeepRunning = false;
                        Console.WriteLine("Goodbye!");
                        break;
                    case "1":
                        GuessTheNumber.GuessTheNumber.Start();
                        break;
                    case "2":
                        Console.WriteLine("Not Implemented Yet!");
                        break;
                    default:
                        Console.WriteLine("Option not recognised.");
                        break;
                }
            }
        }

        private static void PrintMenu()
        {
            var menuOptions = new List<string>(3) {"0: Exit", "1: Guess the Number", "2: Hangman"};
            
            Console.WriteLine("Welcome to Simple Games, Select an option from the list below.");

            for (var i = 0; i < menuOptions.Count; i++)
            {
                Console.WriteLine($"{menuOptions[i]}");
            }
        }
    }
}